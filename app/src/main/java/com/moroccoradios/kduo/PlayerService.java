/**
 * PlayerService.java
 * Implements the app's playback background service
 * The player service plays streaming audio
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.moroccoradios.kduo;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.moroccoradios.kduo.app.MyRadio;
import com.moroccoradios.kduo.core.Station;
import com.moroccoradios.kduo.helpers.EventLogger;
import com.moroccoradios.kduo.helpers.LogHelper;
import com.moroccoradios.kduo.helpers.NotificationHelper;
import com.moroccoradios.kduo.helpers.TrackSelectionHelper;
import com.moroccoradios.kduo.helpers.TransistorKeys;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * PlayerService class
 */


/**
 * PlayerService class
 */
public final class PlayerService extends Service implements
        PlaybackControlView.VisibilityListener, AdsMediaSource.MediaSourceFactory {

    public static final String DRM_SCHEME_UUID_EXTRA = "drm_scheme_uuid";
    public static final String DRM_LICENSE_URL = "drm_license_url";
    public static final String DRM_KEY_REQUEST_PROPERTIES = "drm_key_request_properties";
    public static final String PREFER_EXTENSION_DECODERS = "prefer_extension_decoders";

    public static final String ACTION_VIEW = "com.google.android.exoplayer.demo.action.VIEW";
    public static final String EXTENSION_EXTRA = "extension";

    public static final String ACTION_VIEW_LIST =
            "com.google.android.exoplayer.demo.action.VIEW_LIST";
    public static final String URI_LIST_EXTRA = "uri_list";
    public static final String EXTENSION_LIST_EXTRA = "extension_list";

    private String savedStation;

    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    private Handler mainHandler;
    private EventLogger eventLogger;
    private LinearLayout debugRootView;
    private TextView debugTextView;
    private Button retryButton;

    private DataSource.Factory manifestDataSourceFactory;
    private DataSource.Factory mediaDataSourceFactory;
    private SimpleExoPlayer player;
    private DefaultTrackSelector trackSelector;
    private TrackSelectionHelper trackSelectionHelper;
    private DebugTextViewHelper debugViewHelper;
    public static boolean playerNeedsSource;

    private boolean shouldAutoPlay;
    private int resumeWindow;
    private long resumePosition;
    private Intent intent;


    /* Define log tag */
    private static final String LOG_TAG = PlayerService.class.getSimpleName();


    private static Station mStation;
    private static MediaSessionCompat mSession;
    private int mStationID;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private String mStationMetadata;
    private String mStreamUri;
    private boolean mPlayback;
    private boolean mStationLoading;
    private boolean mStationMetadataReceived;
    private int mPlayerInstanceCounter;
    private int mReconnectCounter;
    private WifiManager.WifiLock mWifiLock;
    private MyRadio myRadio;


    /* Constructor (default) */
    public PlayerService() {
    }

    /* Getter for current station */
    public static Station getStation() {
        return mStation;
    }

    @SuppressLint("WifiManagerLeak")
    @Override
    public void onCreate() {
        super.onCreate();

        // load app state
        loadAppState(getApplication());

        mPlayerInstanceCounter = 0;
        mReconnectCounter = 0;
        mStationMetadataReceived = false;
        myRadio = MyRadio.getInstance();

        // create Wifi lock
        mWifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL, "Transistor_lock");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        shouldAutoPlay = true;
        manifestDataSourceFactory =
                new DefaultDataSourceFactory(
                        this
                        , Util.getUserAgent(this, this.getString(R.string.application_name)));
        mediaDataSourceFactory =
                new DefaultDataSourceFactory(
                        this,
                        Util.getUserAgent(this, this.getString(R.string.application_name)),
                        new DefaultBandwidthMeter());
        mainHandler = new Handler();
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }

        this.intent = intent;
        Log.e("test", "Only test");

        releasePlayer();


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 5000ms
//                showToast("stop player");
                if (player != null) {
//                    releasePlayer();
                }
            }
        }, 10000);


        // checking for empty intent
        if (intent == null) {
            LogHelper.v(LOG_TAG, "Null-Intent received. Stopping self.");
            stopForeground(true); // Remove notification
            stopSelf();
        }

        // ACTION PLAY
        else if (intent.getAction().equals(TransistorKeys.ACTION_PLAY)) {
            LogHelper.v(LOG_TAG, "Service received command: PLAY");

            // get URL of station from intent
            if (intent.hasExtra(TransistorKeys.EXTRA_STATION)) {
                mStation = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
                mStationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
                mStreamUri = mStation.getStreamUri().toString();
            }


            initializePlayer(mStation);

            Intent i = new Intent();
            i.setAction(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
            i.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, TransistorKeys.PLAYBACK_STARTED);
            i.putExtra(TransistorKeys.EXTRA_STATION, mStation);
            i.putExtra(TransistorKeys.EXTRA_STATION_ID, mStationID);
            LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(i);

            mStationLoading = false;
            startPlayback();
            NotificationHelper.update(mStation, mStationID, mStationMetadata, mSession);

        }

        // ACTION STOP
        else if (intent.getAction().equals(TransistorKeys.ACTION_STOP)) {
            LogHelper.v(LOG_TAG, "Service received command: STOP");

            releasePlayer();
            stopPlayback(true);
        }

        // ACTION DISMISS
        else if (intent.getAction().equals(TransistorKeys.ACTION_DISMISS)) {
            LogHelper.v(LOG_TAG, "Service received command: DISMISS");

            // update controller - stop playback
//            mController.getTransportControls().stop();
//            myRadio.stopRadio();
            stopPlayback(true);
            releasePlayer();
        }

        // listen for media button
        MediaButtonReceiver.handleIntent(mSession, intent);

        // default return value for media playback
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        LogHelper.v(LOG_TAG, "onDestroy called.");

        // save state
        mPlayback = false;
        savedStation = "Choose a station below to begin..";
        saveAppState();
        stopForeground(true);
    }

    /* Starts playback */
    private void startPlayback() {

        // set and save state
        mStationMetadata = mStation.getStationName();
        mStationMetadataReceived = false;
        mStation.setPlaybackState(true);
        mPlayback = true;
        mStationLoading = false;
        mStationIDLast = mStationIDCurrent;
        mStationIDCurrent = mStationID;
        savedStation = mStation.getStationName();
        saveAppState();

        // acquire Wifi lock
        if (!mWifiLock.isHeld()) {
            mWifiLock.acquire();
        }


        // send local broadcast
        Intent i = new Intent();
        i.setAction(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        i.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, TransistorKeys.PLAYBACK_LOADING_STATION);
        i.putExtra(TransistorKeys.EXTRA_STATION, mStation);
        i.putExtra(TransistorKeys.EXTRA_STATION_ID, mStationID);
        LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(i);

        // increase counter
        mPlayerInstanceCounter++;


        if (mStreamUri != null) {
//
            // put up notification
            NotificationHelper.show(this, mSession, mStation, mStationID, this.getString(R.string.descr_station_stream_loading));

        }


    }


    /* Stops playback */
    private void stopPlayback(boolean dismissNotification) {


        if (mStation == null) {
            return;
        }
        // set and save state
        mStationMetadata = mStation.getStationName();
        mStationMetadataReceived = false;
        mStation.setPlaybackState(false);
        mPlayback = false;
        mStationLoading = false;
        mStationIDLast = mStationID;
        mStationIDCurrent = -1;
        savedStation = "Choose a station below to begin..";
        saveAppState();

        // release Wifi lock
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }

        // send local broadcast
        Intent i = new Intent();
        i.setAction(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        i.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, TransistorKeys.PLAYBACK_STOPPED);
        i.putExtra(TransistorKeys.EXTRA_STATION, mStation);
        i.putExtra(TransistorKeys.EXTRA_STATION_ID, mStationID);
        LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(i);

        // reset counter
        mPlayerInstanceCounter = 0;

        releasePlayer();


        if (dismissNotification) {
            // dismiss notification
            NotificationHelper.stop();
            // set media session in-active
        } else {
            // update notification
            NotificationHelper.update(mStation, mStationID, mStation.getStationName(), mSession);
            // keep media session active
        }

    }


    /* Release the media player */
    private void releaseMediaPlayer() {

        releasePlayer();

    }


    /* Creates the metadata needed for MediaSession */
    private MediaMetadataCompat getMetadata(Context context, Station station, String metaData) {
        Bitmap stationImage;

        if (station.getStationImage() != null) {
            // use station image
            stationImage = station.getStationImage();
        } else {
            stationImage = null;
        }

        // use name of app as album title
        String albumTitle = context.getResources().getString(R.string.app_name);

        // log metadata change
        LogHelper.i(LOG_TAG, "New Metadata available. Artist: " + station.getStationName() + ", Title: " + metaData + ", Album: " + albumTitle);

        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, station.getStationName())
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, metaData)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, albumTitle)
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, stationImage)
                .build();
    }


    /* Saves state of playback */
    private void saveAppState() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplication());
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, mStationIDCurrent);
        editor.putInt(TransistorKeys.PREF_STATION_ID_LAST, mStationIDLast);
        editor.putBoolean(TransistorKeys.PREF_PLAYBACK, mPlayback);
        editor.putString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, savedStation);
        editor.putBoolean(TransistorKeys.PREF_STATION_LOADING, mStationLoading);
        editor.putString(TransistorKeys.PREF_STATION_METADATA, mStationMetadata);
        editor.apply();
        LogHelper.v(LOG_TAG, "Saving state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + " / " + ")");
    }


    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + ")");
    }


    //// axo player

    private void releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            trackSelector = null;
            trackSelectionHelper = null;
            eventLogger = null;
        }
    }


    @Override
    public void onVisibilityChange(int visibility) {
//        debugRootView.setVisibility(visibility);
    }


    // Internal methods

    private void initializePlayer(Station mStation) {
        if (player == null) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            trackSelectionHelper = new TrackSelectionHelper(trackSelector, videoTrackSelectionFactory);
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
            MediaSource contentMediaSource = buildMediaSource(mStation.getStreamUri());

            player.prepare(contentMediaSource);
            player.setPlayWhenReady(true);

            eventLogger = new EventLogger(trackSelector);
            player.addListener(eventLogger);
            player.setAudioDebugListener(eventLogger);
            player.setVideoDebugListener(eventLogger);
            player.setMetadataOutput(eventLogger);

            player.setPlayWhenReady(shouldAutoPlay);
            playerNeedsSource = true;
        }

    }

    private MediaSource buildMediaSource(Uri uri) {
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory),
                        manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }


    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    // User controls

    private void updateButtonVisibilities() {
//        if (playerNeedsSource) {
//            initializePlayer(mStation);
//            showToast("Retrying to load this station!");
//        }

        if (player == null) {
                        showToast("Retrying to load this station!");

        }

//        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
//        if (mappedTrackInfo == null) {
//            return;
//        }

    }

    @Override
    public MediaSource createMediaSource(Uri uri) {
        return buildMediaSource(uri);
    }

    @Override
    public int[] getSupportedTypes() {
        return new int[]{C.TYPE_DASH, C.TYPE_HLS, C.TYPE_OTHER};
    }
    /**
     * End of inner class
     */


}



